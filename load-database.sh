#!/bin/bash

service mysql start
echo 'downloading data from specified URL'
if [ ! -r /root/import.sql.tar.gz ];then
	curl $url -o /root/import.sql.tar.gz.tmp
	mv /root/import.sql.tar.gz.tmp /root/import.sql.tar.gz
fi

echo 'loading downloaded gz file to database'
tar -vzxOf /root/import.sql.tar.gz | mysql --default-character-set=utf8 && rm /root/import.sql.tar.gz

echo 'creating user and password, with full access'
echo "CREATE USER '$user' IDENTIFIED BY '$password';GRANT ALL PRIVILEGES ON *.* TO '$user'@'%' WITH GRANT OPTION; FLUSH PRIVILEGES;" | mysql --default-character-set=utf8
service mysql stop

echo 'finished loading database'
