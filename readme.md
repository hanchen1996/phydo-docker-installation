
#Pre-requisite:


**Docker** is needed as a virtualbox to encapsulate PhyDo. Installation requires **root** access of your host machine.

**Ubuntu(>=14.04) Install**

Download docker by running `curl -fsSL get.docker.com -o get-docker.sh; sudo sh get-docker.sh`


**Mac Install**

Download docker at (https://docs.docker.com/docker-for-mac/)[https://docs.docker.com/docker-for-mac/] and follow instructions


**Windows**
Unfortunately windows is not supported

#Installation:

download this project and change directory to the project root

run the following script: (It will download ~9Gb of file, it's going to take a while)

```
sudo docker build  -t phydo:current . | tee build.log && sudo docker run -p 31415:8000 -p 31416:5000 --name phydo_container -d --rm phydo:current

```

You now have access to PhyDo running in `localhost:31415`

In case your machine is **rebooted**, restart docker container by running 
```
sudo docker rm -f phydo_container # removes the existing container, if any
# restart the container
sudo docker run -p 31415:8000 -p 31416:5000 --name phydo_container -d --rm phydo:current
```


