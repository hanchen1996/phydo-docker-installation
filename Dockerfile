# Build off PhyDo application using ubuntu base image

FROM ubuntu:14.04

# Install latest updates
RUN apt-get update
RUN apt-get upgrade -y

ENV DEBIAN_FRONTEND noninteractive

# general
RUN apt-get -y install curl git sudo wget
# Install dependencies
RUN curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
# mysql
RUN apt-get -y install mysql-client mysql-server


# phydo backend
RUN apt-get -y install python python-pip

# phydo frontend
RUN apt-get -y install nodejs
# link nodejs -> node
RUN if [ ! -x $(which node) ]; then ln -s /usr/bin/node "$(which nodejs)"; fi

# apache
RUN apt-get -y install apache2 libapache2-mod-wsgi

# uncomment this line when building to enable
# remote access (default is localhost only, we change this
# otherwise our database would not be reachable from outside the container)
# RUN sed -i -e"s/^bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

# Set Standard settings
ENV user phydo
ENV password phydo2017
ENV phydo_backend_dir /var/www/phydo-backend
ENV phydo_frontend_dir /var/www/phydo-frontend
ENV url "https://zenodo.org/record/1034451/files/phydo_db_2018_10_18.sql.tar.gz"

# Manually set up the apache environment variables
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

EXPOSE 5000 8000

ADD load-database.sh /root/load-database.sh
RUN /bin/sh /root/load-database.sh

# installing backend and frontend
RUN git clone --branch latest-release --depth=1 https://hanchen1996@bitbucket.org/doxeylab/phydo-backend.git ${phydo_backend_dir}
# dependencies for requirement.txt
RUN apt-get -y install libmysqlclient-dev python-dev
RUN pip install -r ${phydo_backend_dir}/requirements.txt

RUN echo "\n\
# config file for python, below are MySQL username and password\n\
username='$user'\n\
password='$password'\n\
host='localhost'\n\
database='tree_of_life'\n\
port=3306\n\
" > ${phydo_backend_dir}/config.py

# frontend
RUN git clone --branch latest-release --depth=1 https://hanchen1996@bitbucket.org/doxeylab/phydo-frontend.git ${phydo_frontend_dir}
# build files to public/ folder
RUN cd ${phydo_frontend_dir} && npm install && echo "export const ALLOW_MANUAL_LABEL = false;\n\
export const SERVER_BASE_URL = 'http://localhost:31416';\n" > app/Config.js && npm run build


# setting up apache, replace default conf with our own
ADD apache-config.conf /etc/apache2/sites-enabled/000-default.conf
# listen on 5000 and 8000
RUN echo "Listen 5000\n" >> /etc/apache2/ports.conf
RUN echo "Listen 8000\n" >> /etc/apache2/ports.conf

# By default, simply start apache.

# supervisor to manage processes
RUN apt-get -y install supervisor
ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf
# start supervisor
CMD supervisord
